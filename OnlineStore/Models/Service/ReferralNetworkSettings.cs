﻿using OnlineStore.Models.Entities;

namespace OnlineStore.Models.Service
{
    public class ReferralNetworkSettings
    {
        public int Id { get; set; }
        public string DefaultUserToFollow { get; set; }
        // something else..
    }
}
