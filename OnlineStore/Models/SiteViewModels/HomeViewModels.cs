﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineStore.Models.Entities;

namespace OnlineStore.Models.SiteViewModels
{
    public class HomeViewModels
    {
        public HomeViewModels()
        {
            NewsList = new List<News>();
        }

        public List<News> NewsList { get; set; }
    }
}
