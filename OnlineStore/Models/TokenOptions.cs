﻿
using System;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace OnlineStore.Models
{
    public static class TokenOptions
    {
        public const string Issuer = "OnlineStoreIssuer";
        public const string Audience = "OnlineStoreAudience";
        public static readonly string SecretKey = "Buy_plums_bro!@2017";
        public static SymmetricSecurityKey SigningKey => new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));
        public static TimeSpan Expires = TimeSpan.FromDays(30);
    }
}
