﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Models.Entities
{
    public class Product
    {
        public int Id { get; set; }
        [DisplayName("Название")]
        public string Name { get; set; }
        [DisplayName("Цена")]
        public int Price { get; set; }
        public bool IsAvailable { get; set; }
        [DisplayName("Сумма к распределению")]
        public int AmountToDistribution { get; set; }
        public bool IsDeliverable { get; set; }
        public bool IsInReferralNetwork { get; set; }
        public List<OrderProduct> OrderProducts { get; set; } = new List<OrderProduct>();
    }
}
