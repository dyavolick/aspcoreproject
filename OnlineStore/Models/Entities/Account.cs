﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Models.Entities
{
    public class Account
    {
        public long AccountId { get; set; }
        public string SenderId { get; set; }
        public ApplicationUser Sender { get; set; }
        public bool IsCashbox { get; set; }
        public bool IsCashier { get; set; }
    }
}
