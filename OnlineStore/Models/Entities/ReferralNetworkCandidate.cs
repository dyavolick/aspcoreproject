﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OnlineStore.Models.Entities
{
    public class ReferralNetworkCandidate
    {
        public int Id { get; set; }
        public string UserName { get; set; }

        [Display(Name = "Заявка")]
        [MaxLength(50)]
        public string Comment { get; set; }

        public DateTime RequestedDate { get; set; }
    }
}
