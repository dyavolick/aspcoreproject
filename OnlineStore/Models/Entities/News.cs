﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Models.Entities
{
    public class News
    {
        public int Id { get; set; }
        [DisplayName("Заголовок")]
        public string Title { get; set; }
        [DisplayName("Краткое описание")]
        public string ShortText { get; set; }
        [DisplayName("Текст")]
 
        public string Text { get; set; }
        public DateTime CreatedDate { get; set; }
        public ApplicationUser Creator { get; set; }
    }

}
