﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Models.Entities
{
    public class Follower
    {
        public int FollowerId { get; set; }
        public int? ParentId { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

    }
}
