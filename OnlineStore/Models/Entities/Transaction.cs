﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Models.Entities
{
    public class Transaction
    {
        public string Id { get; set; }
        public int Amount { get; set; }
        public DateTime Datetime { get; set; }
        public string SenderId { get; set; }
        public ApplicationUser Sender { get; set; }
        public string RecipientId { get; set; }
        public ApplicationUser Recipient { get; set; }

        public string TransactionTypeId { get; set; }
        public TransactionType TransactionType { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; }

    }
}
