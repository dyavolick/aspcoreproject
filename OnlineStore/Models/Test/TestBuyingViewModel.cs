﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Models.Test
{
    public class TestBuyingViewModel
    {
        public string login { get; set; }
        public int productId { get; set; }
    }
}
