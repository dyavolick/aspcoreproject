﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Models.Test
{
    public class TestFollower
    {
        public string Name { get; set; }
        public string ParentName { get; set; }
    }
}
