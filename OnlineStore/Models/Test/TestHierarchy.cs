﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Models.Test
{
    public class TestHierarchy
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("children")]
        public List<TestHierarchy> Children { get; set; }
    }
}
