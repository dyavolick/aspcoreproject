﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineStore.Models.Test
{
    public class TestBalance
    {
        public string Name { get; set; }
        public int Amount { get; set; }
    }
}
