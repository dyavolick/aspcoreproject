﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineStore.Data;
using OnlineStore.Models.SiteViewModels;

namespace OnlineStore.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var model = new HomeViewModels();
            model.NewsList = await _context.News.OrderByDescending(m => m.CreatedDate).Take(8).ToListAsync();


            return View(model);
        }

        [Route("r/{inviteCode}")]
        public IActionResult ReferralLink(string inviteCode)
        {
            Response.Cookies.Append("inviteCode", inviteCode, new CookieOptions { Path = "/" });
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Products()
        {
            return View();
        }


        public async Task<IActionResult> News()
        {
            return View(await _context.News.ToListAsync());
        }

        public async Task<IActionResult> NewsView(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var news = await _context.News.SingleOrDefaultAsync(m => m.Id == id);
            if (news == null)
            {
                return NotFound();
            }

            return View(news);
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
