﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineStore.Models;
using OnlineStore.Data;
using OnlineStore.Models.Entities;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OnlineStore.Controllers.Api
{
    [Authorize(ActiveAuthenticationSchemes = "Bearer", Roles = "Administrator")]
    [Produces("application/json")]
    [Route("/api/products")]
    public class ProductsController : Controller
    {
        private readonly ApplicationDbContext _db;

        public ProductsController(ApplicationDbContext dbContext)
        {
            _db = dbContext;
        }

        // GET: api/products
        [HttpGet]
        [AllowAnonymous]
        public IQueryable<Product> GetProducts()
        {
            return _db.Products;
        }

        // GET: api/products/0
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetProduct(int id)
        {
            var product = await _db.Products.FindAsync(id);

            if (product == null)
                return NotFound();

            return Ok(product);
        }

        // POST: api/products
        [HttpPost]
        public async Task<IActionResult> PostProduct([FromBody]Product product)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            _db.Products.Add(product);
            await _db.SaveChangesAsync();

            return StatusCode(201);
        }

        // PUT: api/products/0
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct(int id, [FromBody]Product product)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            //if (id != product.Id)
            //    return BadRequest();

            var productToUpdate = await _db.Products.FindAsync(id);

            if(productToUpdate == null)
                return NotFound();

            productToUpdate.Name = product.Name;
            productToUpdate.Price = product.Price;
            productToUpdate.AmountToDistribution = product.AmountToDistribution;
            productToUpdate.IsAvailable = product.IsAvailable;
            productToUpdate.IsDeliverable = product.IsDeliverable;

            _db.Update(productToUpdate);
            await _db.SaveChangesAsync();

            return StatusCode(204);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            var product = await _db.Products.FindAsync(id);

            if(product == null)
                return NotFound();

            _db.Products.Remove(product);
            await _db.SaveChangesAsync();

            return StatusCode(204);
        }

    }
}
