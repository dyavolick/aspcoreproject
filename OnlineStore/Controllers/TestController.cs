﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OnlineStore.Data;
using OnlineStore.Models.Test;
using OnlineStore.Models.Entities;
using Microsoft.EntityFrameworkCore;
using OnlineStore.Services;

namespace OnlineStore.Controllers
{
    public class TestController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;
        private readonly IEmailSender _emailSender;

        public TestController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context,
            ILoggerFactory loggerFactory,
              IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = loggerFactory.CreateLogger<AccountController>();
            _context = context;
            _emailSender = emailSender;
        }

        public async Task<IActionResult> Index()
        {
            return RedirectToAction("Users");
        }

        public async Task<IActionResult> Users()
        {
            return View();
        }

        public async Task<IActionResult> EditFollowers()
        {
            var userNames = _context.Users.Select(u => u.UserName);
            ViewBag.UserNames = new SelectList(userNames);
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> EditFollowers(TestRegister form)
        {
            if (form.follower == form.follows)
            {
                var userNames = _context.Users.Select(u => u.UserName);
                ViewBag.UserNames = new SelectList(userNames);
                ModelState.AddModelError("", "Нельзя прикреплять самого на себя");
                return View(form);
            }

            var followerUser = _context.Users.FirstOrDefault(u => u.UserName == form.follower);
            var follower = _context.Followers.FirstOrDefault(f => f.UserId == followerUser.Id);

            var newParentUser = _context.Users.FirstOrDefault(u => u.UserName == form.follows);
            var newParentFollower = _context.Followers.FirstOrDefault(f => f.UserId == newParentUser.Id);

            if (newParentFollower == null)
            {
                newParentFollower = new Follower { UserId = newParentUser.Id };
                _context.Followers.Add(newParentFollower);
                _context.SaveChanges();
            }
            else if (follower != null && newParentFollower.ParentId == follower.FollowerId)
            {
                newParentFollower.ParentId = null;
                _context.SaveChanges();
            }

            if (follower == null)
            {
                follower = new Follower { UserId = followerUser.Id, ParentId = newParentFollower.FollowerId };
                _context.Followers.Add(follower);
            }
            else
            {
                follower.ParentId = newParentFollower.FollowerId;
            }

            _context.SaveChanges();

            ViewBag.ParentLogin = form.follows;
            ViewBag.FollowerLogin = form.follower;

            return View("EditFollowersSuccess");
        }



        [HttpPost]
        public async Task<IActionResult> Register(string login)
        {
            var user = new ApplicationUser { UserName = login, Email = login + "@test.ru" };
            var result = await _userManager.CreateAsync(user, "Qwerty##45");
            if (result.Succeeded)
            {

                await _signInManager.SignInAsync(user, isPersistent: false);
                _logger.LogInformation(3, "User created a new account with password.");
            }

            ViewBag.NewUserLogin = login;

            return View("RegisterSuccess");
        }

        public async Task<IActionResult> Buying()
        {
            var userNames = _context.Users.Select(u => u.UserName);
            ViewBag.UserNames = new SelectList(userNames);
            var products = _context.Products.Select(p => new
            {
                Id = p.Id,
                Value = $"{p.Name}, цена {p.Price}, для начислений {p.AmountToDistribution}"
            }).ToList();
            ViewBag.Products = new SelectList(products, "Id", "Value");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Buying(string login, int productId)
        {
            var product = await _context.Products.FindAsync(productId);
            var AmountToAccruals = product.AmountToDistribution / 6;

            var usersReceivedAccruals = new List<string>();
            var user = _context.Users.FirstOrDefault(u => u.UserName == login);

            var order = new Order { User = user,Datetime = DateTime.Now };

            _context.Orders.Add(order);
            _context.SaveChanges();

            var orderProducts = new OrderProduct { Order = order, Product = product };
            _context.OrderProducts.Add(orderProducts);
            _context.SaveChanges();


            ChargeAccrual(usersReceivedAccruals, user, 1, AmountToAccruals, order);
            _context.SaveChanges();

            // create transaction to PremiumFund
            var membersAccrualsSum = _context.Transactions.Where(t => t.OrderId == order.Id).Sum(t => t.Amount);
            var premiumFundUser = _context.Users.FirstOrDefault(u => u.UserName == "PremiumFund");

            var fundTransaction = new Transaction
            {
                RecipientId = premiumFundUser.Id,
                Amount = product.AmountToDistribution - membersAccrualsSum,
                Datetime = DateTime.Now,
                TransactionTypeId = "02",
                Order = order
            };
            _context.Transactions.Add(fundTransaction);
            _context.SaveChanges();

            ViewBag.UserNames = usersReceivedAccruals;
            return View("BuyingSuccess");
        }

        private void ChargeAccrual(List<string> usersReceivedAccruals, ApplicationUser user, int layer, int amountToAccruals, Order order)
        {
            var parentFollowerId = _context.Followers.FirstOrDefault(f => f.UserId == user.Id)?.ParentId;
            if (parentFollowerId != null)
            {
                var parentFollower = _context.Followers.FirstOrDefault(f => f.FollowerId == parentFollowerId);
                var parentUser = _context.Users.FirstOrDefault(u2 => u2.Id == parentFollower.UserId);

                var transaction = new Transaction
                {
                    RecipientId = parentUser.Id,
                    Amount = amountToAccruals,
                    Datetime = DateTime.Now,
                    TransactionTypeId = "02",
                    Order = order
                };

                _context.Transactions.Add(transaction);
                usersReceivedAccruals.Add(parentUser.UserName);

                if (layer < 5)
                {
                    layer++;
                    ChargeAccrual(usersReceivedAccruals, parentUser, layer, amountToAccruals, order);
                }
            }
        }

        public async Task<IActionResult> Followers()
        {
            var users = _context.Users.ToList();
            var followers = _context.Followers.ToList();
            var result = new List<TestFollower>();

            foreach (var u in users)
            {
                var parentFollowerId = followers.FirstOrDefault(f => f.UserId == u.Id)?.ParentId;
                if (parentFollowerId != null)
                {
                    var parentFollower = followers.FirstOrDefault(f => f.FollowerId == parentFollowerId);
                    var parentUserName = users.FirstOrDefault(u2 => u2.Id == parentFollower.UserId).UserName;
                    result.Add(new TestFollower { Name = u.UserName, ParentName = parentUserName });
                }
                else
                {
                    result.Add(new TestFollower { Name = u.UserName });
                }
            };

            return View(result);
        }

        public async Task<IActionResult> Balances()
        {
            var users = _context.Users.ToList();
            var transactions = _context.Transactions.ToList();
            var result = new List<TestBalance>();

            foreach (var u in users)
            {
                var userBalance = transactions.Where(t => t.RecipientId == u.Id).ToList().Sum(t => t.Amount);

                result.Add(new TestBalance { Name = u.UserName, Amount = userBalance });
            };

            return View(result);
        }

        public async Task<IActionResult> Hierarchy()
        {
            var users = _context.Users.ToList();
            var followers = _context.Followers.OrderBy(f => f.ParentId).ToList();
            var followersNames = new List<TestFollower>();

            var fisrtFollower = followers.FirstOrDefault(f => f.ParentId == null);
            var firstUser = users.FirstOrDefault(u => u.Id == fisrtFollower?.UserId);

            var result = new TestHierarchy { Name = firstUser.UserName };

            while (followers.Count > 0)
            {

            }




            foreach (var u in users)
            {
                var parentFollowerId = followers.FirstOrDefault(f => f.UserId == u.Id)?.ParentId;
                if (parentFollowerId != null)
                {
                    var parentFollower = followers.FirstOrDefault(f => f.FollowerId == parentFollowerId);
                    var parentUserName = users.FirstOrDefault(u2 => u2.Id == parentFollower.UserId).UserName;


                }
            };

            return View(followersNames);
        }

        public IActionResult EditDefaultUserToFollow()
        {
            var userNames = _context.Users.Select(u => u.UserName);
            ViewBag.UserNames = new SelectList(userNames);
            return View("EditDefaultUserToFollow");
        }

        [HttpPost]
        public async Task<IActionResult> EditDefaultUserToFollow(string username)
        {
            var settings = _context.ReferralNetworkSettings.First();
            settings.DefaultUserToFollow = username;

            _context.ReferralNetworkSettings.Update(settings);
            await _context.SaveChangesAsync();
            return View();
        }

        [Route("test/referral")]
        public IActionResult ReferralNetworkCandidates()
        {
            ViewBag.Candidates = _context.ReferralNetworkCandidates;
            return View("ReferralNetworkCandidates");
        }

        [HttpPost]
        [Route("test/referral-submit")]
        public async Task<IActionResult> SubmitReferralCandidate(string username)
        {
            var candidate = _context.ReferralNetworkCandidates.FirstOrDefault(c => c.UserName == username);
            var user = _context.Users.FirstOrDefault(u => u.UserName == username);

            if (candidate == null || user == null)
                return BadRequest();

            user.ReferralLink = user.Id; // unique referral link; Id for the first time

            _context.Users.Update(user);
            _context.ReferralNetworkCandidates.Remove(candidate);

            await _context.SaveChangesAsync();


            var location = new Uri($"{Request.Scheme}://{Request.Host}");
            var url = location.AbsoluteUri;
            var refLinkUrl = url + $"r/{user.Id}";// Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code, returnUrl = returnUrl }, protocol: Request.Url.Scheme);
            await _emailSender.SendEmailAsync(user.Email, "Администрация сайта", $"Ваша реферальная сслка ссылка: {refLinkUrl}");



            return Ok();
        }

        public async Task<IActionResult> History(string id)
        {
            var user = _context.Users.FirstOrDefault(u=>u.UserName == id);
            var transactions = _context.Transactions
                .Include(t=>t.Order)
                    .ThenInclude(o => o.User)
                .Include(t => t.Order)
                    .ThenInclude(o => o.OrderProducts)
                .Where(t=>t.Recipient == user).ToList();

            var products = _context.Products.ToList();

            foreach (var t in transactions)
            {
                t.Order.OrderProducts.ForEach(orderProduct =>
                {
                    orderProduct.Product = products.FirstOrDefault(product => product.Id == orderProduct.ProductId);
                });
            }

            return View(transactions);
        }
    }
}

