﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace OnlineStore.Controllers.Api
{
    [Authorize(ActiveAuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/test")]
    public class TestController : Controller
    {
        [HttpGet]
        [Route("anon")]
        [AllowAnonymous]
        public string Anon()
        {
            return "anonymous";
        }

        [Route("user")]
        [HttpGet]
        public string Authorize()
        {
            return "authorize";
        }

        [Route("admin")]
        [Authorize(ActiveAuthenticationSchemes = "Bearer", Roles = "Administrator")]
        [HttpGet]
        public string AuthorizeAdmin()
        {
            return "authorize admin";
        }
    }
}
