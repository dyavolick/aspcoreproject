﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using OnlineStore.Models.Entities;
using OnlineStore.Data;
using OnlineStore.Models.Service;
using System.Collections.Generic;

namespace OnlineStore.Configuration
{
    public class DataInitializer
    {
        public static async Task Seed(IServiceProvider serviceProvider)
        {
            await SeedAccountSettings(serviceProvider);
            await SeedProducts(serviceProvider);
            await SeedTransactionTypes(serviceProvider);
            await SeedTestOrders(serviceProvider);
            await SeedTestNews(serviceProvider);
        }

        private static async Task SeedTransactionTypes(IServiceProvider serviceProvider)
        {
            var dbContext = serviceProvider.GetRequiredService<ApplicationDbContext>();

            if (!dbContext.TransactionTypes.Any())
            {
                await dbContext.TransactionTypes.AddRangeAsync(new List<TransactionType>
                {
                    new TransactionType
                    {
                        TransactionTypeId = "01",
                        Name = "Покупка"
                    },
                    new TransactionType
                    {
                        TransactionTypeId = "02",
                        Name = "Начисление"
                    },

                }
                );
            }

            await dbContext.SaveChangesAsync();
        }

        public static async Task SeedAccountSettings(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();

            if (!await roleManager.RoleExistsAsync("Administrator"))
            {
                await roleManager.CreateAsync(new IdentityRole("Administrator"));
            }


            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();

            var dbContext = serviceProvider.GetRequiredService<ApplicationDbContext>();


            var admin = dbContext.Users.FirstOrDefault(u => u.UserName == "Admin");

            if (admin == null)
            {
                admin = new ApplicationUser
                {
                    UserName = "Admin",
                    Email = "love.beer@gmail.com",
                    PhoneNumber = "8093993210",
                    FirstName = "Admin",
                    MiddleName = "Adminovich",
                    LastName = "Adminov"
                };
                await userManager.CreateAsync(admin, "Admin@1");
            }

            await userManager.AddToRoleAsync(admin, "Administrator");


            if (dbContext.ReferralNetworkSettings.FirstOrDefault() == null)
                dbContext.ReferralNetworkSettings.Add(new ReferralNetworkSettings { DefaultUserToFollow = "Admin" });

            await dbContext.SaveChangesAsync();

            var premiumFund = dbContext.Users.FirstOrDefault(u => u.UserName == "PremiumFund");

            if (premiumFund == null)
            {
                premiumFund = new ApplicationUser
                {
                    UserName = "PremiumFund",
                    Email = "PremiumFund@test.com",
                };
                await userManager.CreateAsync(premiumFund, "Admin@1");
            }
        }

        public static async Task SeedProducts(IServiceProvider serviceProvider)
        {
            var dbContext = serviceProvider.GetRequiredService<ApplicationDbContext>();

            if (!dbContext.Products.Any())
            {
                dbContext.Products.Add(new Product
                {
                    Name = "Коробка слив",
                    Price = 2000,
                    AmountToDistribution = 600,
                    IsAvailable = true,
                    IsDeliverable = true,
                    IsInReferralNetwork = true
                });

                dbContext.Products.Add(new Product
                {
                    Name = "5 коробок слив",
                    Price = 6500,
                    AmountToDistribution = 1500,
                    IsAvailable = true,
                    IsDeliverable = true,
                    IsInReferralNetwork = true
                });

                dbContext.Products.Add(new Product
                {
                    Name = "Кисель из красных водорослей",
                    Price = 3000,
                    AmountToDistribution = 2100,
                    IsAvailable = true,
                    IsDeliverable = true,
                    IsInReferralNetwork = true
                });
            }

            await dbContext.SaveChangesAsync();
        }

        public static async Task SeedTestOrders(IServiceProvider serviceProvider)
        {
            var dbContext = serviceProvider.GetRequiredService<ApplicationDbContext>();
            var admin = dbContext.Users.FirstOrDefault(u => u.UserName == "Admin");

            if (dbContext.Orders.FirstOrDefault() != null)
                return;

            var order = new Order
            {
                User = admin,
                Datetime = DateTime.Now
            };

            var product1 = dbContext.Products.FirstOrDefault(p => p.Name == "Коробка слив");
            var product2 = dbContext.Products.FirstOrDefault(p => p.Name == "5 коробок слив");

            order.OrderProducts.AddRange(new List<OrderProduct>
            {
                new OrderProduct
                {
                    Order = order,
                    Product = product1
                },
                new OrderProduct
                {
                    Order = order,
                    Product = product2
                }
            });

            dbContext.Orders.Add(order);

            await dbContext.SaveChangesAsync();
        }

        public static async Task SeedTestNews(IServiceProvider serviceProvider)
        {
            var dbContext = serviceProvider.GetRequiredService<ApplicationDbContext>();
            var admin = dbContext.Users.FirstOrDefault(u => u.UserName == "Admin");

            if (dbContext.News.Any())
                return;

            dbContext.News.Add(new News()
            {
                Title = "Тестовая новость 1",
                ShortText = "Тестовая новость 1 Тестовая новость 1 Тестовая новость 1 Тестовая новость 1",
                Text = "Тестовая новость 1 Тестовая новость 1 Тестовая новость 1 Тестовая новость 1",
                CreatedDate = DateTime.Now,
                Creator = admin
            });
            dbContext.News.Add(new News()
            {
                Title = "Тестовая новость 2",
                ShortText = "Тестовая новость 2 Тестовая новость 2 Тестовая новость 2 Тестовая новость 2",
                Text = "Тестовая новость 2 Тестовая новость 2 Тестовая новость 2 Тестовая новость 2",
                CreatedDate = DateTime.Now,
                Creator = admin
            });
            dbContext.News.Add(new News()
            {
                Title = "test news 3",
                ShortText = "Тестовая новость 3 Тестовая новость 3 Тестовая новость 3 Тестовая новость 3 Тестовая новость 3 Тестовая новость 3 Тестовая новость 3 Тестовая новость 3",
                Text = "Тестовая новость 3 Тестовая новость 3 Тестовая новость 3 Тестовая новость 3 Тестовая новость 3 Тестовая новость 3 Тестовая новость 3 Тестовая новость 3",
                CreatedDate = DateTime.Now,
                Creator = admin
            });
            dbContext.News.Add(new News()
            {
                Title = "Тестовая новость 4",
                ShortText = "Тестовая новость 4 Тестовая новость 4 Тестовая новость 4 Тестовая новость 4 Тестовая новость 4 Тестовая новость 4 Тестовая новость 4 Тестовая новость 4",
                Text = "Тестовая новость 4 Тестовая новость 4 Тестовая новость 4 Тестовая новость 4 Тестовая новость 4 Тестовая новость 4 Тестовая новость 4 Тестовая новость 4",
                CreatedDate = DateTime.Now,
                Creator = admin
            });

            await dbContext.SaveChangesAsync();
        }




    }

}
