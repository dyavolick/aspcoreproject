﻿using System.Linq;
using System.Threading.Tasks;
using OnlineStore.Data;
using OnlineStore.Models.Entities;
using OnlineStore.Models.Test;

namespace OnlineStore.Utils
{
    public static class AccountUtilities
    {
        public static async Task EditFollowers(ApplicationDbContext dbContext, TestRegister form)
        {
            if (form.follower == form.follows)
                return;

            var followerUser = dbContext.Users.FirstOrDefault(u => u.UserName == form.follower);
            var follower = dbContext.Followers.FirstOrDefault(f => f.UserId == followerUser.Id);

            var newParentUser = dbContext.Users.FirstOrDefault(u => u.UserName == form.follows);
            var newParentFollower = dbContext.Followers.FirstOrDefault(f => f.UserId == newParentUser.Id);

            if (newParentFollower == null)
            {
                newParentFollower = new Follower { UserId = newParentUser.Id };
                dbContext.Followers.Add(newParentFollower);
                await dbContext.SaveChangesAsync();
            }
            else if (follower != null && newParentFollower.ParentId == follower.FollowerId)
            {
                newParentFollower.ParentId = null;
                await dbContext.SaveChangesAsync();
            }

            if (follower == null)
            {
                follower = new Follower { UserId = followerUser.Id, ParentId = newParentFollower.FollowerId };
                dbContext.Followers.Add(follower);
            }
            else
            {
                follower.ParentId = newParentFollower.FollowerId;
            }

            await dbContext.SaveChangesAsync();
        }
    }
}
