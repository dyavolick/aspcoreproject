﻿$(document).ready(function () {
    $('.accept-referral-btn').click(function () {
        $(this).hide();
        var username = $(this).closest("tr") // Finds the closest row <tr> 
            .find(".username") // Gets a descendent with class="nr"
            .text(); // Retrieves the text within <td>

        $.ajax({
            type: "POST",
            url: "/test/referral-submit",
            data: {
                username: username
            },
            error: function (data) {
                $(this).show();
            }
        });
    });
});