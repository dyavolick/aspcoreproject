﻿$(document).ready(function () {
    // for the time being..
    $.ajax({
        type: "POST",
        url: "/api/token",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({ "username": "Admin", "password": "Admin@1" }),
        success: function (data) {
            localStorage.setItem('access_token', data.access_token);
        }
    });
    //

    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('access_token'));
        }
    });

    refreshTable();

    var handleOk = function (data, textStatus, jqXHR) {
        alert('ok');
        refreshTable();
    }


    $('#postBtn').click(function () {
        $.ajax({
            type: "POST",
            url: "/api/products",
            contentType: 'application/json; charset=utf-8',
            data: getJsonData(),
            success: function (data) {
                refreshTable();
            }
        });
    });

    $('#putBtn').click(function () {
        var id = $('#productId').val();

        $.ajax({
            type: "PUT",
            url: "/api/products/" + id,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: getJsonData(),
            success: function (data) {
                refreshTable();
            }
        });
    });

    $('#delBtn').click(function () {
        var id = $('#productId').val();

        $.ajax({
            type: "DELETE",
            url: "/api/products/" + id,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data) {
                refreshTable();
            }
        });
    });

    function getJsonData() {
        var name = $('#name').val(),
            price = $('#price').val(),
            amountToDistribution = $('#amountToDistribution').val(),
            isAvailable = $('#isAvailable')[0].checked,
            isInReferralNetwork = $('#isInReferralNetwork')[0].checked;

        return JSON.stringify({
            "name": name,
            "price": price,
            "amountToDistribution": amountToDistribution,
            "isAvailable": isAvailable,
            "isInReferralNetwork": isInReferralNetwork
        });
    }

    function refreshTable() {
        $('#productsTable').html('');

        $.ajax({
            type: "GET",
            url: "/api/products",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (products) {
                $.each(products, function (index, product) {
                    var tableRow = "<tr>"
                        + "<td>" + product.id + "</td>"
                        + "<td>" + product.name + "</td>"
                        + "<td>" + product.price + "</td>"
                        + "<td>" + product.amountToDistribution + "</td>"
                        + "<td>" + product.isAvailable + "</td>"
                        + "<td>" + product.isInReferralNetwork + "</td>"
                        + "</tr>";

                    $('#productsTable').append(tableRow);
                });
            }
        });
    }
});

